use super::*;

#[derive(Debug)]
pub enum ProgramOp {
	Add(i32), // a combination of + and -
	Move(i32), // a combination of < and >
	Loop(u32), // for [ withaddress of ] in code
	LoopEnd(u32), // for ] with address of [ in code
	Read(u32), // for any number of ,
	Write(u32), // for any number of .
}

pub struct ProgramObject {
	pub ops: Vec<ProgramOp>,
}

impl ProgramObject {

	fn new() -> ProgramObject {
		ProgramObject { ops: Vec::with_capacity(64) }
	}

	pub fn from_src_op_vec(src_ops: &Vec<SourceOp>) -> ProgramObject {
		let mut prog = ProgramObject::new();

		// contains where in code each [ is
		// it works like a stack because [ and ] match and nest
		let mut loop_entries_stack = Vec::<u32>::with_capacity(256);

		let ops = &mut prog.ops;

		for op in src_ops {
			match op {
				SourceOp::Inc => {
					if let Some(ProgramOp::Add(prog_op)) = ops.last_mut() {
						*prog_op += 1;
						if *prog_op == 0 {
							ops.pop();
						}
					} else {
						ops.push(ProgramOp::Add(1));
					}
				},
				SourceOp::Dec => {
					if let Some(ProgramOp::Add(prog_op)) = ops.last_mut() {
						*prog_op -= 1;
						if *prog_op == 0 {
							ops.pop();
						}
					} else {
						ops.push(ProgramOp::Add(-1));
					}
				},
				SourceOp::Next => {
					if let Some(ProgramOp::Move(prog_op)) = ops.last_mut() {
						*prog_op += 1;
						if *prog_op == 0 {
							ops.pop();
						}
					} else {
						ops.push(ProgramOp::Move(1));
					}
				},
				SourceOp::Prev => {
					if let Some(ProgramOp::Move(prog_op)) = ops.last_mut() {
						*prog_op -= 1;
						if *prog_op == 0 {
							ops.pop();
						}
					} else {
						ops.push(ProgramOp::Move(-1));
					}
				},
				SourceOp::Loop => {
					// remember the addres of this [
					loop_entries_stack.push(ops.len() as u32);

					// place 0 for now as dummy
					// when we encounter the matching ]
					// it will be filled with an actual address
					ops.push(ProgramOp::Loop(0));
				},
				SourceOp::EndLoop => {
					if let Some(addr) = loop_entries_stack.pop() {
						let ops_len = ops.len() as u32; // avoid brwchkr growling
						if let ProgramOp::Loop(addr_end) = &mut ops[addr as usize] {
							// the address of matching ] for this [
							*addr_end = ops_len;

							// addr refers to address of matching [
							ops.push(ProgramOp::LoopEnd(addr));
						} else {
							panic!("Expected ProgramOp::Loop, found: {:?}", ops[addr as usize]);
						}
					} else {
						panic!("One or more ']' doesn't have a matching '[' !");
					}
				},
				SourceOp::Read => {
					if let Some(ProgramOp::Read(num)) = ops.last_mut() {
						*num += 1;
					} else {
						ops.push(ProgramOp::Read(1));
					}
				},
				SourceOp::Write => {
					if let Some(ProgramOp::Write(num)) = ops.last_mut() {
						*num += 1;
					} else {
						ops.push(ProgramOp::Write(1));
					}
				},
			}
		}

		// check if some [ didn't have a match
		if loop_entries_stack.len() != 0 {
			panic!("One or more '[' don't have a matching ']' !");
		}

		prog
	}
}