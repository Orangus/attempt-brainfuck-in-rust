#[derive(Copy, Clone)]
pub enum SourceOp {
	Inc,		// +
	Dec,		// -
	Next,		// >
	Prev,		// <
	Loop,		// [
	EndLoop,	// ]
	Read, 		// ,
	Write, 		// .
}

impl SourceOp {
	pub fn vec_from_source(code: &str) -> Vec<SourceOp> {
		let mut s = Vec::with_capacity(code.chars().count());
		for ch in code.chars() {
			s.push(match ch {
				'+' => SourceOp::Inc,
				'-' => SourceOp::Dec,
				'>' => SourceOp::Next,
				'<' => SourceOp::Prev,
				'[' => SourceOp::Loop,
				']' => SourceOp::EndLoop,
				',' => SourceOp::Read,
				'.' => SourceOp::Write,
				_ => continue,
			});
		}

		s
	}
}