pub mod source_opcode;
pub mod program_object;

pub use self::source_opcode::*;
pub use self::program_object::*;

use std::io;
use std::io::Write;

pub struct Context {
	data: Vec<i8>,
	ptrmask: usize,
	getcher: getch::Getch,
}

impl Context {
	pub fn new() -> Context {
		Context {
			data: vec![0; 1024 * 4],
			ptrmask: 1024 * 4 - 1,
			getcher: getch::Getch::new(),
		}
	}

	pub fn eval(&mut self, code: &str) {
		let opcodes = SourceOp::vec_from_source(code);

		let ops = ProgramObject::from_src_op_vec(&opcodes).ops;

		let mut op_ptr: usize = 0;
		let mut data_ptr: usize = 0;

		// Bitwise-and('&') is a turbo-fast equivalent of modulo('%')
		// But works only when you want take modulo of a power-of-two
		let ptrmask = self.ptrmask;

		while op_ptr < ops.len() {
			match ops[op_ptr] {
				ProgramOp::Add(val) => {
					self.data[data_ptr] = (self.data[data_ptr] as i32 + val as i32) as i8;
				},
				ProgramOp::Move(offset) => {
					// smth & ptrmask == smth % (ptrmask + 1)
					data_ptr = (data_ptr as isize + offset as isize) as usize & ptrmask;
				},
				ProgramOp::Loop(addr) => {
					if self.data[data_ptr] == 0 {
						op_ptr = addr as usize;
					}
				},
				ProgramOp::LoopEnd(addr) => {
					if self.data[data_ptr] != 0 {
						op_ptr = addr as usize;
					}
				},
				ProgramOp::Read(times) => {
					for _ in 0..times {
						self.data[data_ptr] = self.getcher.getch().unwrap() as i8;
					}
				},
				ProgramOp::Write(times) => {
					let mut c = self.data[data_ptr];
					if c < 0 {
						c = 0;
					}
					let c = (c as u8) as char;
					for _ in 0..times {
						print!("{}", c);
					}
					io::stdout().flush().unwrap();
				},
			}

			op_ptr += 1;
		}
	}
}