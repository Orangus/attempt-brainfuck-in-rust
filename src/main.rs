use brainfuck::bf;

fn main() {
	// the program that prints 'Hello, World!' =)
	let code = "+[-->-[>>+>-----<<]<--<---]>-.>>>+.>
				>..+++[.>]<<<<.+++.------.<<-.>>>>+.";

	let mut bf_ctx = bf::Context::new();

	bf_ctx.eval(code);
}